// class containing methods to animate elements on the page
class Anim {

    // animate moving the piece
    static move_piece(session, node, target) {
        // create data objects for the nodes
        node = { e: document.getElementById(node), }
        target = { e: document.getElementById(target), }

        // get positional data
        const nodeRect = node.e.getBoundingClientRect()
        node.y = nodeRect.top
        node.x = nodeRect.left

        const targetRect = target.e.getBoundingClientRect()
        target.y = targetRect.top
        target.x = targetRect.left

        const x_offset = Math.floor(Math.abs(node.x - target.x))
        const y_offset = Math.floor(Math.abs(node.y - target.y))

        let x, y
        node.x < target.x ? x = x_offset
            : target.x < node.x ? x = -x_offset
            : x = 0

        node.y < target.y ? y = y_offset
            : target.y < node.y ? y = -y_offset
            : y = 0

        // store the image representing the chess piece
        const piece = node.e.firstChild

        // start the animation
        piece.addEventListener('transitionend', Anim.move_end(session, target))
        piece.style.transform = `translate(calc(-50% + ${x}px), ${y}px)`
        piece.style.zIndex = 1
        piece.classList.add('moving')
    }

    // finish up the animation
    static move_end(session, target) {
        const handler = function() {
            // remove the event listener
            this.removeEventListener('transitionend', handler)
            // remove attributes and classes
            this.classList.remove('moving')
            this.style.transform = ""
            this.style.zIndex = ""
            // append the node
            target.e.append(this)
            // set the game state to ready
            session.ready = true
        }
        return handler
    }

    // add a fade-out animation for the piece being removed
    static remove_piece(node) {
        node = document.getElementById(node).firstChild
        node.addEventListener('transitionend', Anim.remove_end())
        node.classList.add('fade-out')
    }

    // finish up the animation
    static remove_end() {
        const handler = function() {
            this.removeEventListener('transitionend', handler)
            this.parentNode.removeChild(this)
        }
        return handler
    }

}