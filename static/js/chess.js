class Game {
    
    // initialise the game instance
    constructor() {
        // set the instance props
        this._API_PREFIX_ = location.toString().split('/')[0] + "//" + location.host + "/api/game/"
        this.player = new Player(this, 'white')
        this.ready = true
        this.turn = null
        this.movesets = null
        this.controls = {
            'new_game': document.getElementById('start'),
        }

        // run initial methods
        this.create_board()
        this.player.init()

        // set up the events
        this.controls.new_game.addEventListener('click', this.create_new_game(this))
    }

    // updates the board
    update_board(data) {
        if (data.capture) {
            Anim.remove_piece(data.to)
        }
        Anim.move_piece(this, data.from, data.to)
        this.movesets = data.moves
        this.turn = data.turn
        console.log('SESSION_TURN', this.turn)
    }

    // create the board elements
    create_board() {
        const board = document.getElementById('board')
        const rows = [8,7,6,5,4,3,2,1]
        const columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

        // create board elements
        rows.forEach(function(row, row_index) {
            // create a row element
            const r = document.createElement('div')
            r.classList.add('board-row')
            columns.forEach(function(column, column_index) {
                // create a grid element
                const grid = document.createElement('div')
                grid.id = column + row
                grid.classList.add('board-grid')
                r.appendChild(grid)
            })
            board.appendChild(r)
        })
    }

    // create a new game, and clean up any existing elements
    create_new_game(self) {
        return function() {
            console.clear()
            // clear any existing elements from the board
            Array.prototype.slice.call(document.querySelectorAll('.board-grid > img')).forEach(function(image) {
                image.parentNode.removeChild(image)
            })
            // create new elements for the board
            fetch(self._API_PREFIX_ + "start")
            .then(response => response.json())
            .then(data => {
                // process response data
                console.log('GAME_START_RESPONSE', data)
                if (data.response == 200) {
                    // create the images for the pieces
                    data.board.forEach(row => {
                        row.forEach(grid => {
                            if (grid.piece) {
                                const element = document.createElement('img')
                                element.classList.add('piece')
                                element.src = grid.piece.image
                                element.setAttribute('data-player', grid.piece.player)
                                document.getElementById(grid.id).appendChild(element)
                            }
                        })
                    })
                    // update the game properties
                    self.turn = data.turn
                    self.movesets = data.moves
                    self.ready = true
                }
                // display error message
                else { console.log(data.err_message) }
            })
            .catch(function(){
                // process the error
                console.log(arguments)
            })
        }
    }
}