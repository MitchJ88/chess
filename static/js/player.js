class Player {

    // initialise the player instance
    constructor(session, player='white') {
        this.session = session
        this.player = player
        this.selected = null
    }

    // initialise all methods
    init() {
        const nodeList = document.querySelectorAll('.board-grid')
        Array.prototype.slice.call(nodeList).forEach(node => {
            node.addEventListener('click', this.gridClickHandler(this))
        })
    }

    // handles the grid clicks
    gridClickHandler(self) {
        return function(){
            // do nothing if...
            if (self.turn != self.player || !self.ready) return

            // store some information about this grid item
            const occupied = this.firstChild
            let target = null
            occupied ? target = this.firstChild.getAttribute('data-player') : null

            // call the responding methods based on the conditions
            if (target == self.player && this != self.selected) {
                // set a selection
                self.select(this)
            } else if (self.selected && !this.classList.contains('move')) {
                // deselect the selection
                self.deselect()
            } else if (self.selected && this.classList.contains('move')) {
                // send a server request to move the selected piece
                self.request_move(self.selected, this)
                self.deselect()
            }
        }
    }

    // select the node given as argument to the method
    select(node) {
        if (this.selected) {
            // deselect the current selection
            this.deselect()
        }
        // set a new selection
        this.selected = node
        node.classList.add('selected')
        this.highlight_moves(node.id)
    }

    // deselect the current selection
    deselect() {
        const node = document.querySelector('.selected')
        node.classList.remove('selected')
        this.selected = null
        this.highlight_moves(null)
    }

    // set or remove all highlights for moves
    highlight_moves(id) {
        if (id) {
            // highlight all the available moves
            const player = this.player
            const moves = this.session.movesets[player][id]
            moves.forEach(function(move) {
                const target = document.getElementById(move.position)
                target.classList.add('move')
                if (move.capture) {
                    target.classList.add('capture')
                }
            })
        } else {
            // remove all highlights
            const elements = Array.prototype.slice.call(document.querySelectorAll('.move'))
            elements.forEach(function(element) {
                element.classList.remove('capture')
                element.classList.remove('move')
            })
        }
    }

    // send a server request to make a move
    request_move(node_from, node_to) {
        // store grid data
        const data = {
            'from': node_from.id,
            'to': node_to.id,
        }
        console.log('REQUEST_MOVE', data)

        // send server request
        fetch(this._API_PREFIX_ + "move", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        })
        .then(response => response.json())
        .then(data => {
            // process the response data
            console.log('REQUEST_MOVE_RESPONSE', data)
            if (data.response == 200) {
                // update the board
                this.ready = false
                this.session.update_board(data)
                // pass the turn to the AI when the game state is ready
                const loop = function() {
                    if (this.ready) {
                        clearInterval(await)
                        this.ready = false
                        this.await_turn()
                    }
                }
                const await = setInterval(loop.bind(this), 50)
            } 
            else { console.log(data.err_message) }
        })
        // process request error
        .catch(function(){
            console.log(arguments)
        })
    }

    // let the server know it's the AIs turn
    await_turn() {
        console.log('AWAITING_TURN')
        fetch(this._API_PREFIX_ + "comp")
        .then(response => response.json())
        .then(data => {
            // process the response data
            console.log("AWAITING_TURN_RESPONSE", data)
            this.session.update_board(data)
        })
    }

    //++++ GETTERS AND SETTERS ++++\\
    
    get _API_PREFIX_() {
        return this.session._API_PREFIX_
    }

    get turn() {
        return this.session.turn
    }

    get ready() {
        return this.session.ready
    }

    set ready(bool) {
        this.session.ready = bool
    }

    set turn(player) {
        this.session.turn = player
    }

    get movesets() {
        return this.session.movesets
    }

    set movesets(data) {
        this.session.movesets = data
    }
}

