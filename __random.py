import random

# set min and max range values and assign a random number within this range
min_value = 1
max_value = 50
random_number = random.randint(min_value, max_value)

# create a variable to keep track of how many guesses the player made
attempt_count = 0

# store the phrase that the player will be prompted with
phrase = "Guess a number between " + str(min_value) + " and " + str(max_value) + ": "

# create a game loop
while True:
    # Provide instructions and prompt the player for input
    print("\nPress 'q' at any time to quit.")
    guessed_number = input(phrase)

    # validate the player's input
    if guessed_number == 'q':
        # exit the program by breaking out of the game loop
        print("\nYou gave up. Exiting the game.")
        break
    else:
        # convert the input to an integer
        try:
            guessed_number = int(guessed_number)
            # increment the attempt count if the input value was within the valid range
            if (guessed_number >= min_value and guessed_number <= max_value):
                attempt_count += 1
            # continue to the next loop iteration if input is outside the valid range
            else:
                print("That number is out of the valid range!")
                continue
        # handle the exception when the input cannot be converted to an integer
        except:
            print("That's not a number.")
            continue

    # provide hints for wrong guesses
    if guessed_number != random_number:
        off_by_value = abs(guessed_number - random_number)
        if off_by_value <= 5:
            print('Hint: Very warm.')
        elif off_by_value <= 10:
            print('Hint: Warm.')
        elif off_by_value <= 20:
            print('Hint: Cold.')
        else:
            print('Hint: Very cold.')
    # confirm the guessed number is correct and show the amount of attempts it took
    else:
        print("\nCorrect! The number was", str(random_number) + '.')
        print("It took you " + str(attempt_count) + " guesses to find the right number.")
        break