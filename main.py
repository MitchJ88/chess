from flask import Flask, render_template, request, jsonify
app = Flask(__name__)

import json

# custom modules
from api.game import Game
from api.player import Player
from api.ai import AI


DEBUG_FALSE = False

@app.route('/')
@app.route('/home')
def home():
    return render_template('index.html', title='Home')

@app.route('/api/game/start', methods=['GET'])
def start():
    if request.method == 'GET':
        return jsonify(Game.start())

@app.route('/api/game/move', methods=['POST'])
def move():
    if request.method == 'POST':
        data = request.get_json()
        return jsonify(Player.request_move(data['from'], data['to']))

@app.route('/api/game/comp', methods=['GET'])
def ai_move():
    if request.method == 'GET':
        return jsonify(AI.temp())

if __name__ == '__main__':
    if DEBUG_FALSE:
        app.run(host='192.168.178.15', port=5000)
    else:
        app.run(debug=True)
