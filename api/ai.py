from api.session import Session as session
from api.moveset import Moves
from api.game import Game

# temp code
import random

class AI:
    """Class containing methods for the AI."""

    def temp():
        """Comment here."""

        # get session data
        session_data = session.load()
        # get the corresponding moveset
        if session_data['turn'] == 'black':
            movesets = session_data['moves']['black']
        else:
            movesets = session_data['moves']['white']
        # get a random move
        response = AI.random_move(session_data, movesets)
        Game.update_session(session_data, save=True)
        response['moves'] = session_data['moves']
        return response

    def random_move(session_data, moveset):
        """Does a random move."""
        moves = []
        for key in moveset:
            for move in moveset[key]:
                data = {'from': key, 'to': move['position']}
                moves.append(data)

        idx = random.randint(0, len(moves)-1)
        move = moves[idx]
        return Moves.execute_move(session_data, move['from'], move['to'])
