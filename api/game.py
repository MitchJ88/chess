# custom modules
from api.session import Session as session
from api.moveset import Moves

class Game:
    """This class contains all the game related methods."""

    def start():
        """Sets up a new game."""
        session_data = {
            'response': 200,
            'status': 'running',
            'turn': 'white',
        }
        board_data = Game.create_board()
        session_data['board'] = board_data['board']
        session_data['board_map'] = board_data['board_map']
        Game.update_session(session_data, save=True)
        return session_data


    def update_session(session_data, save=False):
        """Updates the game data."""
        Game.set_moves(session_data)
        if save:
            session.save(session_data)
        return
        

    def set_moves(session_data):
        """Grab and store all the movesets for each chess piece."""
        moves = {'black': {}, 'white': {}}
        board_map = session_data['board_map']
        for grid_id in board_map:
            grid = board_map[grid_id]
            piece = grid['piece']
            if piece != None:
                if piece['player'] == 'black':
                    moves['black'][grid_id] = Moves.get_moveset(session_data, grid_id)
                else:
                    moves['white'][grid_id] = Moves.get_moveset(session_data, grid_id)

        session_data['moves'] = moves

    
    def create_board():
        """Creates a board for a new game."""
        rows = ['8', '7', '6', '5', '4', '3', '2', '1']
        columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
        board = []
        board_map = {}
        
        for y in range(0, len(rows)):
            new_row = []
            board.append(new_row)
            for x in range(0, len(columns)):
                row = rows[y]
                col = columns[x]
                grid = {
                    'id': col + row,
                    'piece': Game.get_piece(row, col),
                    'x': x,
                    'y': y,
                }
                new_row.append(grid)
                board_map[grid['id']] = grid

        return {
            'board': board,
            'board_map': board_map,
        }


    def create_piece(piece_type, player):
        """Creates a dictionary for the piece."""
        prefix = "../static/images/chess/" + piece_type
        if player == 'white':
            suffix = '_w.png'
        else:
            suffix = '_b.png'

        piece = {
            'class': piece_type,
            'player': player,
            'moved': 0,
            'image': prefix + suffix,
            'captured': 0,
        }

        return piece


    def get_piece(row, column):
        """Finds the piece for corresponding grid."""
        grid_map = {
            'A': 'rook',
            'B': 'knight',
            'C': 'bishop',
            'D': 'king',
            'E': 'queen',
            'F': 'bishop',
            'G': 'knight',
            'H': 'rook',
        }

        if row == '7':
            return Game.create_piece('pawn', 'black')
        elif row == '2':
            return Game.create_piece('pawn', 'white')
        elif row == '8':
            return Game.create_piece(grid_map[column], 'black')
        elif row == '1':
            return Game.create_piece(grid_map[column], 'white')
        else:
            return None