class Moves:
    """Classes containg move related methods."""

    def get_moveset(session_data, grid_id):
        """Returns a complete moveset for the given chess piece on the board."""

        # store sections of the session data
        board = session_data['board']
        board_map = session_data['board_map']
        grid = board_map[grid_id]
        # store the list coordinates
        x = grid['x']
        y = grid['y']
        # store the piece information
        piece = grid['piece']
        class_type = piece['class']

        # switch case for the corresponding moveset function
        switcher = {
            'pawn': Moves.moveset_pawn(board, y, x),
            'rook': Moves.moveset_rook(board, y, x),
            'knight': Moves.moveset_knight(board, y, x),
            'bishop': Moves.moveset_bishop(board, y, x),
            'king': Moves.moveset_king(board, y, x),
            'queen': Moves.moveset_queen(board, y, x),
        }
        moveset = switcher[class_type]
        # return the moveset
        return moveset

    
    def validate_move(moveset, move_from, move_to):
        """Checks if the requested move is valid."""
        for player in moveset: 
            movelist = moveset[player].get(move_from)
            if movelist:
                for move in movelist:
                    if move['position'] == move_to:
                        return True

        return False


    def execute_move(session_data, move_from, move_to):
        """Updates the grid piece data and returns a response."""

        board_map = session_data['board_map']
        # store the grid data
        grid_f = board_map[move_from]
        grid_t = board_map[move_to]
        # check if piece is being captured
        if grid_t['piece'] != None:
            capture = True
            grid_f['piece']['captured'] += 1
        else:
            capture = False
        # update the grid pieces
        grid_f['piece']['moved'] += 1
        grid_t['piece'] = grid_f['piece']
        grid_f['piece'] = None
        # update the turn attribute
        if session_data['turn'] == 'black':
            session_data['turn'] = 'white'
        else:
            session_data['turn'] = 'black'
        # return response
        return {
            'response': 200,
            'from': move_from,
            'to': move_to,
            'capture': capture,
            'turn': session_data['turn']
        }


    def moveset_pawn(board, y, x):
        """Returns the valid moves for the given piece."""

        piece = board[y][x]['piece']
        player = piece['player']
        moves = []
        
        # assign the direction value
        if player == 'black':
            d = 1
        else:
            d = -1

        # check the board
        if (y+d) < 8 and (y+d) >= 0:
            # check for vertical moves
            grid = board[y+d][x]
            if grid['piece'] == None:
                Moves.append_move(moves, grid['id'], False)
                if y+(d*2) < 8 and y+(d*2) >= 0:
                    grid = board[y+(d*2)][x]
                    if piece['moved'] == 0 and grid['piece'] == None:
                        Moves.append_move(moves, grid['id'], False)
            
            # check for diagional capture moves
            if (x+1) < 8:
                grid = board[y+d][x+1]
                if grid['piece'] != None and grid['piece']['player'] != player:
                    Moves.append_move(moves, grid['id'], True)

            if (x-1) >= 0:
                grid = board[y+d][x-1]
                if grid['piece'] != None and grid['piece']['player'] != player:
                    Moves.append_move(moves, grid['id'], True)

        return moves


    def moveset_rook(board, y, x):
        """Returns the valid moves for the given piece."""

        piece = board[y][x]['piece']
        player = piece['player']
        moves = []

        def add_move(y_range, x_range):
            for row in y_range:
                for col in x_range:
                    grid = board[row][col]
                    if grid['piece'] == None:
                        Moves.append_move(moves, grid['id'], False)
                    elif grid['piece']['player'] != player:
                        Moves.append_move(moves, grid['id'], True)
                        return
                    else:
                        return

        # move up
        y_range = reversed(range(0, y))
        x_range = range(x, x+1)
        add_move(y_range, x_range)
        # move down
        y_range = range(y+1, 8)
        x_range = range(x, x+1)
        add_move(y_range, x_range)
        # move right
        y_range = range(y, y+1)
        x_range = range(x+1, 8)
        add_move(y_range, x_range)
        # move left
        y_range = range(y, y+1)
        x_range = reversed(range(0, x))
        add_move(y_range, x_range)
        
        return moves


    def moveset_knight(board, y, x):
        """Returns the valid moves for the given piece."""

        piece = board[y][x]['piece']
        player = piece['player']
        moves = []

        def add_move(grid):
            if grid['piece'] == None:
                Moves.append_move(moves, grid['id'], False)
            else:
                if grid['piece']['player'] != player:
                    Moves.append_move(moves, grid['id'], True)

        # move: 2 down
        if y+2 < 8:
            if x+1 < 8:
                add_move(board[y+2][x+1])
            if x-1 >= 0:
                add_move(board[y+2][x-1])
        # move: 2 up
        if y-2 >= 0:
            if x+1 < 8:
                add_move(board[y-2][x+1])
            if x-1 >= 0:
                add_move(board[y-2][x-1])
        # move: 1 down
        if y+1 < 8:
            if x+2 < 8:
                add_move(board[y+1][x+2])
            if x-2 >= 0:
                add_move(board[y+1][x-2])
        # move: 1 up
        if y-1 >= 0:
            if x+2 < 8:
                add_move(board[y-1][x+2])
            if x-2 >= 0:
                add_move(board[y-1][x-2])

        return moves


    def moveset_bishop(board, y, x):
        """Returns the valid moves for the given piece."""

        piece = board[y][x]['piece']
        player = piece['player']
        moves = []

        # loop over the possible grids
        def add_move(y_range, d):
            x_offset = 1
            for row in y_range:
                if d == 'left':
                    col = x - x_offset
                else:
                    col = x + x_offset
                
                if col < 8 and col >= 0:
                    grid = board[row][col]
                    if grid['piece'] == None:
                        Moves.append_move(moves, grid['id'], False)
                    elif grid['piece']['player'] != player:
                        Moves.append_move(moves, grid['id'], True)
                        return
                    else:
                        return
                else:
                    return

                x_offset += 1

        # move: up, left
        y_range = reversed(range(0, y))
        add_move(y_range, 'left')
        # move: up, right
        y_range = reversed(range(0, y))
        add_move(y_range, 'right')
        # move: down, left
        y_range = range(y+1, 8)
        add_move(y_range, 'left')
        # move: down, right
        y_range = range(y+1, 8)
        add_move(y_range, 'right')
        
        return moves


    def moveset_king(board, y, x):
        """Returns the valid moves for the given piece."""

        piece = board[y][x]['piece']
        player = piece['player']
        moves = []

        y_range = range(y-1, y+2)
        x_range = range(x-1, x+2)

        # loop over all the adjacent grid items
        for row in y_range:
            for col in x_range:
                if row < 8 and row >= 0 and col < 8 and col >= 0:
                    if row == y and col == x:
                        continue
                    else:
                        grid = board[row][col]
                        if grid['piece'] == None:
                            Moves.append_move(moves, grid['id'], False)
                        else:
                            if grid['piece']['player'] != player:
                                Moves.append_move(moves, grid['id'], True)
                            else:
                                continue
                else:
                    continue

        return moves


    def moveset_queen(board, y, x):
        """Returns the valid moves for the given piece."""

        piece = board[y][x]['piece']
        player = piece['player']
        moves = []

        # straight directions loop
        def add_move(y_range, x_range):
            for row in y_range:
                for col in x_range:
                    grid = board[row][col]
                    if grid['piece'] == None:
                        Moves.append_move(moves, grid['id'], False)
                    elif grid['piece']['player'] != player:
                        Moves.append_move(moves, grid['id'], True)
                        return
                    else:
                        return

        # diagional directions loop
        def add_diagional_move(y_range, d):
            x_offset = 1
            for row in y_range:
                # set offset for the x-axis
                if d == 'left':
                    col = x - x_offset
                else:
                    col = x + x_offset
                # check if column index is within grid bounds
                if col < 8 and col >= 0:
                    grid = board[row][col]
                    if grid['piece'] == None:
                        Moves.append_move(moves, grid['id'], False)
                    elif grid['piece']['player'] != player:
                        Moves.append_move(moves, grid['id'], True)
                        return
                    else:
                        return
                else:
                    return
                # increment offset value
                x_offset += 1

        # move up
        y_range = reversed(range(0, y))
        x_range = range(x, x+1)
        add_move(y_range, x_range)
        # move down
        y_range = range(y+1, 8)
        x_range = range(x, x+1)
        add_move(y_range, x_range)
        # move right
        y_range = range(y, y+1)
        x_range = range(x+1, 8)
        add_move(y_range, x_range)
        # move left
        y_range = range(y, y+1)
        x_range = reversed(range(0, x))
        add_move(y_range, x_range)

        # move: up, left
        y_range = reversed(range(0, y))
        add_diagional_move(y_range, 'left')
        # move: up, right
        y_range = reversed(range(0, y))
        add_diagional_move(y_range, 'right')
        # move: down, left
        y_range = range(y+1, 8)
        add_diagional_move(y_range, 'left')
        # move: down, right
        y_range = range(y+1, 8)
        add_diagional_move(y_range, 'right')

        return moves


    def append_move(target_list, target_id, capture):
        """Appends a move to the list."""
        target_list.append({
            'position': target_id,
            'capture': capture,
        })
