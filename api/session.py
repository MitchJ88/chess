import json

class Session:
    """Class that contains methods to save and load game sessions."""

    def save(data):
        """Saves the game session data to the .json file"""
        while True:
            try:       
                f = open("data/game_session.json", "w")
                f.write(json.dumps(data))
            except:
                print("++++ ERR: Unable to write session data to file. ++++")
            else:
                f.close()
                return True


    def load():
        """Loads the game session data and returns it."""
        while True:
            try:
                f = open("data/game_session.json", "r")
                data = f.read()
            except:
                print("++++ ERR: Unable to read session data from file. ++++")
                return False
            else:
                f.close()
                data = json.loads(data)
                Session.sync_dict_to_list(data)
                return data

    
    def sync_dict_to_list(session_data):
        """
        Passes all the dictionary references to the list data
        to sync everything up again.
        """
        board = session_data['board']
        board_map = session_data['board_map']
        for grid_id in board_map:
            grid = board_map[grid_id]
            x = grid['x']
            y = grid['y']
            board[y][x] = grid
