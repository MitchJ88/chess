from api.session import Session as session
from api.moveset import Moves
from api.game import Game


class Player:
    """Class containing methods for executing player tasks."""

    def request_move(move_from, move_to):
        """Validates data needed to process the move."""

        # load session data
        session_data = session.load()
        if session_data:
            board = session_data['board']
        else:
            print("++++ ERR: Unable to get session data in Player.request_move(args) ++++")
            return {
                'reponse': 400,
                'err_message': "ERR: Unable to get session data in Player.request_move(args)",
            }
        # store all the movesets and validate the requested move
        moveset = session_data['moves']
        valid_move = Moves.validate_move(moveset, move_from, move_to)
        if valid_move:
            # process the move and return a reponse to the client
            response = Moves.execute_move(session_data, move_from, move_to)
            Game.update_session(session_data, save=True)
            response['moves'] = session_data['moves']
            return response
        else:
            # pass back an error
            return {
                'response': 400,
                'err_message': "ERR: Invalid move requested.",
            }
